# Image Classification Server 

MNIST Classification Server with Keras and Docker


## Goals:

1. Making a REST API definition with OpenAPI or Swagger

2. Creating a Docker container to create a repeatable build environment

3. Making predictions with our API and posting images over HTTP


### REST API definition

Let's begin by defining the REST API. This is comprised of four activities: getting the project source code from GitHub with git; installing the necessary packages and reviewing the packages that will be needed in order to run our server; editing and creating the OpenAPI or Swagger definition file in YAML; and then finally handling a POST-ed image in that code that the REST API takes to turn an actual image file into a tensor.

First, we need to clone the repository that we've provided in order to have a REST service. I'm getting this over HTTPS and cloning it with the command line:

```bash
$ git clone https://gitlab.com/bcesgim/image-classification-server.git
```
